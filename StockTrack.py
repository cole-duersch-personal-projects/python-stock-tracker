import yfinance as yf
import time



aPortFile = open("portfolio.txt", "a")
rPortFile = open("portfolio.txt", "r")
filepath = "portfolio.txt"


stockStr = rPortFile.readlines()
stock1str = stockStr[1]
stock2str = stockStr[2]
stock3str = stockStr[3]

stock1 = yf.Ticker(stock1str).info
stock1price = stock1['regularMarketPrice']

amt1 = rPortFile.readlines(5)
amt2 = rPortFile.readlines(6)
amt3 = rPortFile.readlines(7)



market1 = yf.Ticker('^DJI').info
market2 = yf.Ticker('^IXIC').info
market3 = yf.Ticker('^GSPC').info

market1price = market1['regularMarketPrice']
market2price = market2['regularMarketPrice']
market3price = market3['regularMarketPrice']

market1close = market1['regularMarketPreviousClose']
market2close = market2['regularMarketPreviousClose']
market3close = market3['regularMarketPreviousClose']

m1cent = ((market1price - market1close) / market1close) * 100
m2cent = ((market2price - market2close) / market2close) * 100
m3cent = ((market3price - market3close) / market3close) * 100

#
def Markets():
    if m1cent > 0 and m2cent > 0 and m3cent > 0:
        print("The markets are up today!")
    
    if m1cent < 0 and m2cent < 0 and m3cent < 0:
        print("The markets are down today!")
    
    if m1cent < 0 or m2cent < 0 or m3cent < 0:
        print("The markets are mixed today")
    
    print(f"Dow Jones Industrial: $ {market1price:.2f} \tPrevious Close Price: $ {market1close:.2f} \tPercent Change: {m1cent:.2f}%")
    print()
    print(f"NASDAQ: $ {market2price:.2f} \tPrevious Close Price: $ {market2close:.2f} \tPercent Change: {m2cent:.2f}%")
    print()
    print(f"S&P500 : $ {market3price:.2f} \tPrevious Close Price: $ {market3close:.2f} \tPercent Change: {m3cent:.2f}%")
def Portfolio():
    pass


def Email():
    pass


def Help():
    pass


def updatePort():
    pass


def portSetup():
    print("Welcome to Portfolio Setup!")
    s1 = input("Please enter your first stock ticker: ")
    s2 = input("Please enter your second stock ticker: ")
    s3 = input("Please enter your third stock ticker: ")
    a1 = input(f"Please enter the amount of {s1} held: ")
    a2 = input(f"Please enter the amount of {s2} held: ")
    a3 = input(f"Please enter the amount of {s3} held: ")


    aPortFile.write("TICKERS\n")
    aPortFile.write(s1 + "\n")
    aPortFile.write(s2 + "\n")
    aPortFile.write(s3 + "\n")

    aPortFile.write("AMOUNTS\n")
    aPortFile.write(a1 + "\n")
    aPortFile.write(a2 + "\n")
    aPortFile.write(a3 + "\n")

    print("Your portfolio is set up!")
    time.sleep(3)
    Portfolio()




def mainMenu():
   resp = input("What do you want do? ")
   if resp == "markets": Markets()
   if resp == "portfolio": Portfolio()
   if resp == "email": Email()
   if resp == "help": Help()
   if resp == "update": updatePort()
   if resp == "Setup": portSetup()
   if resp == "exit" or "quit":
       print("Happy Trading!")
       exit()
def Startup():
    print("Hi")
    time.sleep(1)
    print("Welcome to PyStock market tracker and email service")

mainMenu()